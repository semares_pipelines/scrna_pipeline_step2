# scrna_pipeline_step2

## What is it

This is a pipeline to process single cell data after alignment using Seurat. The pipeline is based on nextflow and uses the docker container for R execution environment. The pipeline can be used either with Semares as well as stand-alone.

The pipeline includes the following steps:
- Basic quality check
- Normalization
- Identification of highly variable genes
- Scaling
- Dimensionality reduction
- Clustering
- Detection of cluster markers

## Standalone usage

For standalone usage, install `nextflow` and run the pipeline:

```
nextflow run https://gitlab.com/semares_pipelines/scrna_pipeline_step2.git -profile docker --input <input_folder> --output <output_folder>
```

The `<input_folder>` should contain exactly one .h5 file containing scRNA count matrix (from CellRanger, pipeline step1).

The `<output_folder>` should be empty. As a result, it will contain:

- 	`NormCounts.tsv` - count matrix (from Seurat)
- 	`metadata.tsv` - metadata matrix
- 	`umap_ResolutionNN.tsv` - dimensionality reduction matrix (umap), where NN is a resolution (currently 0.5)
-   `var_genes.tsv` - a list of most higly variable genes (currently 2000)
-   `SeuObj_Analysed.rds` - Seurat object containing the results
-   `*.html`    - files containing various QC graphs

## Semares usage

To use the pipeline with Semares, You need the following steps:

- go to `persistance/workflow_adapter`
- clone the pipeline:
```
git clone https://gitlab.com/semares_pipelines/scrna_pipeline_step2.git
```
- add a record to the `pipelines.yaml`:
```
- pipelineName: scrna_pipeline_step2
  pipelineDescription: second step scRNA pipeline using Seurat
  pipelinePath: <semares_base>/persistence/workflow_adapter/scrna_pipeline_step2/main.nf
  pipelineCommand: nextflow run -r main -profile docker
  pipelineParams:
  - paramName: Input dataset
    paramKey: --input
    paramDescription: input dataset
    paramType: InputPath
    isMultiValue: false
    isRequired: true
  - paramName: Output Folder Path
    paramDescription: output path
    paramKey: --output
    paramType: OutputPath
    isMultiValue: false
    isRequired: true
```

## Future work

- parameters for the main settings (filtering settings, scale factor, number of variable genes, number of neighbours and resolution for UMAP and clustering)
- single HTML/PDF report

