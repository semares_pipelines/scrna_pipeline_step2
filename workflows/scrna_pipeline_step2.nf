/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    IMPORT LOCAL MODULES/SUBWORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

include { SEURAT_WORKFLOW } from '../subworkflows/local/seurat_workflow'

def checkPathParamList = [
    params.output, params.metadata_file
]
for (param in checkPathParamList) { if (param) { file(param, checkIfExists: true) } }

input_ch = Channel.fromList(params.input.tokenize(","))
           .map{ file(it, checkIfExists: true) }
           .collect()
           .view()

// general input and params
// ch_input = file(params.input)
ch_metadata = file(params.metadata_file)

workflow SCRNA_PIPELINE_STEP2 {
    SEURAT_WORKFLOW (
         input_ch,
         ch_metadata
    )
}