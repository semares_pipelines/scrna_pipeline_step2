process SEURAT_PROCESS {

    container "${(params.containers_local) ? 'semares_scpipeline_2:2.0.0' : 
              'registry.gitlab.com/semares_pipelines/scrna_pipeline_step2/semares_scpipeline_2:2.0.0' }"

    input:
        path input_folder
        path metadata

    output:
        path("NormCounts.tsv"), emit: count_matrix
        path("metadata.tsv"), emit: metadata_matrix
        path("umap_*"), emit: dimensionality_reduction
        path("var_genes.tsv"), emit: variable_genes
        path("*.png"), emit: png
//        path("*.html"), emit: html
        path("SeuObj_Analysed.rds"), emit: seurat
    
    script:
        """
        seurat_pipeline.R \\
           --input '${input_folder.join(",")}' \\
           --output "./" \\
           --metadata_file '$metadata' \\
           --perc_mito_threshold ${params.perc_mito_threshold} \\
           --min_num_genes ${params.min_num_genes} \\
           --max_num_genes ${params.max_num_genes} \\
           --umap_resolution_value ${params.umap_resolution_value} \\
           --min_pct ${params.min_pct} \\
           --logfc_threshold ${params.logfc_threshold} \\
           --var_features_method ${params.var_features_method} \\
           --var_features_num ${params.var_features_num} \\
           --normalization_method ${params.normalization_method} \\
           --normalization_scale ${params.normalization_scale}
        """  
}
