include {SEURAT_PROCESS} from "../../modules/local/seurat/seurat.nf"

workflow SEURAT_WORKFLOW {
   take:
      input
      metadata
   main:
      SEURAT_PROCESS(
          input,
          metadata
      )
}